//
//  ViewController.swift
//  Project2
//
//  Created by Andrew Famiano on 2/27/20.
//  Copyright © 2020 Waystocreate. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    
    var countries = [String]()
    var score = 0
    var correctAnswer = 0
    var questionCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        button1.layer.borderWidth = 1
        button2.layer.borderWidth = 1
        button3.layer.borderWidth = 1
        
        button1.layer.borderColor = UIColor.lightGray.cgColor
        button2.layer.borderColor = UIColor.lightGray.cgColor
        button3.layer.borderColor = UIColor.lightGray.cgColor
        
        countries += ["estonia", "france", "germany", "ireland", "italy", "monaco", "nigeria", "poland", "russia", "spain","uk", "us"]
        
        askQuestion()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareTapped))
    }
    
    func askQuestion(action: UIAlertAction! = nil) {
        countries.shuffle()
        correctAnswer = Int.random(in: 0...2)
        questionCount += 1
        
        button1.setImage(UIImage(named: countries[0]), for: .normal)
        button2.setImage(UIImage(named: countries[1]), for: .normal)
        button3.setImage(UIImage(named: countries[2]), for: .normal)
        
        title = countries[correctAnswer].uppercased() + " " + "-" + " " + "Score: \(score)"
    }
    
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        
        var titleController: String
        var titleAction: String
        var message: String
        
        if sender.tag == correctAnswer {
            titleController = "Correct"
            score += 1
        } else {
            titleController = "Wrong, correct answer is flag \(correctAnswer + 1)"
            score -= 1
        }
        
        if questionCount == 10 {
            titleController = "Game over"
            titleAction = "Play again"
            message = "Your final score is \(score)"
            score = 0
            questionCount = 0
            correctAnswer = 0
        } else {
            titleAction = "Continue"
            message = "Your score is \(score)"
        }
    
        let ac = UIAlertController(title: titleController, message: message, preferredStyle: .alert)
               
        ac.addAction(UIAlertAction(title: titleAction, style: .default, handler: askQuestion))
               
        present(ac, animated: true)
       
    }
    
    @objc func shareTapped() {
       let ac = UIAlertController(title: "Your score is \(score)", message: nil, preferredStyle: .alert)
       ac.addAction(UIAlertAction(title: "Continue", style: .default, handler: nil))
                     
       present(ac, animated: true)
    }
}

